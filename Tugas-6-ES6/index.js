// Soal 1
console.log("\n--- Soal 1 --- \n");

const luas = (panjang,lebar) => panjang * lebar
const keliling = (panjang,lebar) => 2*panjang + 2*lebar

console.log(luas(2,6));  // Expected 12
console.log(keliling(2,6));  // Expected 16


// Soal 2
console.log("\n--- Soal 2 --- \n");

const newFunction = (firstName, lastName) => 
({
    firstName,
    lastName,
    fullName() {
       console.log(`${firstName} ${lastName}`); 
    }
})
    


    
//Driver Code 
newFunction("William", "Imoh").fullName()


// Soal 3
console.log("\n--- Soal 3 --- \n");

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  } 
  
const {firstName, lastName,address,hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)

// Soal 4
console.log("\n--- Soal 4 --- \n");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]

//Driver Code
console.log(combined)

// Soal 5
console.log("\n--- Soal 5 --- \n");
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

let after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`

console.log(before);
console.log(after);
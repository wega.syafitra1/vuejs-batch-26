// Soal 1
console.log("\n--- Soal 1 --- \n");

var nilai = 76;
console.log("Nilai yang didapat: " + nilai);

if(nilai >= 85) console.log("Indeks A");
else if (nilai >= 75 && nilai <85) console.log("Indeks B");
else if (nilai >= 65 && nilai <75) console.log("Indeks C");
else if (nilai >= 55 && nilai <65) console.log("Indeks D");
else console.log("Indeks E");



// Soal 2
console.log("\n--- Soal 2 --- \n");

var tanggal = 13;
var bulan = 12;
var tahun = 2002;

var strBulan;
switch(bulan){
    case 1: {strBulan = "Januari"; break;}
    case 2: {strBulan = "Februari"; break;}
    case 3: {strBulan = "Maret"; break;}
    case 4: {strBulan = "April"; break;}
    case 5: {strBulan = "Mei"; break;}
    case 6: {strBulan = "Juni"; break;}
    case 7: {strBulan = "Juli"; break;}
    case 8: {strBulan = "Agustus"; break;}
    case 9: {strBulan = "September"; break;}
    case 10: {strBulan = "Oktober"; break;}
    case 11: {strBulan = "November"; break;}
    case 12: {strBulan = "Desember"; break;}
    default: {strBulan = "suatu bulan";}
}

var out = tanggal + " " + strBulan + " " + tahun;
console.log(out);



// Soal 3
console.log("\n--- Soal 3 --- \n");

var n = 5;
var strBaris ="";

// Loop menampilkan semua segitiga
for(var i=1; i<= n; i++){
    // Loop menampilkan per-baris
    for (var x = 1; x <= i; x++){
        strBaris+= "#";}
    console.log(strBaris);
    strBaris = "";  // Reset
}




// Soal 4
console.log("\n--- Soal 4 --- \n");

var m = 10;
var baris = 1;
var counter = 1;

while (counter <= 10){
    var step = baris % 4;
    if (step == 1) console.log(counter++ + " - I love programming");
    else if (step == 2) console.log(counter++ + " - I love Javascript");
    else if (step == 3) console.log(counter++ + " - I love VueJS");
    else if (step == 0) {

        // Tiap 4 baris bikin pembatas
        // Loop menampilkan pembatas

        for (var x = 1; x <counter; x++){
            strBaris+= "=";}
        console.log(strBaris);
        strBaris = "";  // Reset
    }  

    baris++;
}
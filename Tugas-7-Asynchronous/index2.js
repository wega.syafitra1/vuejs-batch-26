var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
var time = 10000

const run = async() =>{
    for (var i=0;i<books.length; i++){
        await readBooksPromise(time, books[i]).then(value => time = value)
    }    
}
run()

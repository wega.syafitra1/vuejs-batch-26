// Soal 1
console.log("\n--- Soal 1 --- \n");

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// bubblesort method
function bubbleSort(array){
    for (var i=0; i < array.length-1; i++){
        for(var x=0; x< array.length-i-1; x++){
            if (array[x].localeCompare(array[x+1]) > 0) {
                // swap elements
                var temp = array[x];
                array[x] = array[x+1];
                array[x+1] = temp;
            }
        }
    }
    return array;
}
var sorted = bubbleSort(daftarHewan);
console.log(sorted.join("\n"));

// Soal 2
console.log("\n--- Soal 2 --- \n");

function introduce(item) {
    // asumsi item tidak undefined
    return "Nama saya "+item.name + ", umur saya " + item.age + " tahun, alamat saya di " + item.address
            + ", dan saya punya hobby yaitu " + item.hobby + "!";
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3
console.log("\n--- Soal 3 --- \n");

function hitung_huruf_vokal(str){
    var count = 0;
    var lowstr = str.toLowerCase();
    var vowels = ["a", "i", "u", "e", "o"];
    for(var i=0; i< lowstr.length ; i++){
        if (vowels.includes(lowstr.charAt(i)) ) count++;
    }

    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// Soal 4
console.log("\n--- Soal 4 --- \n");

function hitung(int){
    return int*2-2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
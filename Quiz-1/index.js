// Soal 1
console.log("\n--- Soal 1 --- \n");

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"

function jumlah_kata(kalimat){
    var array = kalimat.split(" ");
    var count = 0;
    var new_array = []
    // Eliminasi whitespace
    for (var i =0; i<array.length ; i++){
        if (array[i] == ""){
            continue;
        }
        new_array.push(array[i])
    }
    console.log(new_array.length); 
}

jumlah_kata(kalimat_1);
jumlah_kata(kalimat_2);


// Soal 2
console.log("\n--- Soal 2 --- \n");
var tanggal = 29
var bulan = 2
var tahun = 2020

function next_date(tanggal,bulan,tahun) {
    var strBulan = "";
    var lenBulan = 0;
    switch(bulan){
        case 1: {lenBulan = 31; break;}
        case 2: {lenBulan = 28; break;}
        case 3: {lenBulan = 31; break;}
        case 4: {lenBulan = 30; break;}
        case 5: {lenBulan = 31; break;}
        case 6: {lenBulan = 30; break;}
        case 7: {lenBulan = 31; break;}
        case 8: {lenBulan = 31; break;}
        case 9: {lenBulan = 30; break;}
        case 10: {lenBulan = 31; break;}
        case 11: {lenBulan = 30; break;}
        case 12: {lenBulan = 31; break;}
        default: {lenBulan = 1;}
    }
    // Februari pada tahun kabisat
    if ((tahun%4 == 0) && (bulan == 2)){
        lenBulan =29;
    }

    // Menambah tanggal
    tanggal++
    // Berubah bulan
    if (tanggal>lenBulan){
        tanggal = 1;
        bulan++;

        // Berubah tahun 
        if (bulan > 12){
            tahun++;
            bulan = 1;
        }
    }
    

    // Changes bulan from int to string
    switch(bulan){
        case 1: {strBulan = "Januari"; break;}
        case 2: {strBulan = "Februari"; break;}
        case 3: {strBulan = "Maret"; break;}
        case 4: {strBulan = "April"; break;}
        case 5: {strBulan = "Mei"; break;}
        case 6: {strBulan = "Juni"; break;}
        case 7: {strBulan = "Juli"; break;}
        case 8: {strBulan = "Agustus"; break;}
        case 9: {strBulan = "September"; break;}
        case 10: {strBulan = "Oktober"; break;}
        case 11: {strBulan = "November"; break;}
        case 12: {strBulan = "Desember"; break;}
        default: {strBulan = "suatu bulan";}
    }

    
    // returns string 
    console.log(tanggal + " " + strBulan + " " + tahun); 
}

next_date(tanggal,bulan,tahun); 
next_date(31,12,2020);
next_date(28,2,2021);
next_date(28,2,2020);
